package utils;

import java.io.FileInputStream;
import java.util.Properties;

public class ConfigReader {

    public static Properties properties = new Properties();

    static {

        try {

            String path = "config.properties";
            FileInputStream fileInputStream = new FileInputStream(path);
            properties.load(fileInputStream);
            fileInputStream.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static String getProperty(String key){
        return properties.getProperty(key);
    }
}
