@api
Feature: Validate CRUD

  Scenario Outline: Validating the GoRest API CRUD operation
    Given Create user with "<name>", "<job>"
    And Validate that status code is 201
    And Make GET call to get user with "<URL>"
    And Validate that status code is 200
    And Updating the user with the following data
      | name | neo       |
      | job  | chosenOne |
    And Validate that status code is 200
    When I delete user
    Then Validate that status code is 204

    Examples: GoRest data
      | name    | job   | URL                           |
      | Trinity | lover | https://reqres.in/api/users/2 |
